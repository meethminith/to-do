package com.meethjain.todo.model;

/**
 * Created on 26/07/17.
 *
 * @author Meeth D Jain
 */

public class Todo {

  private String mId;
  private String mTitle;
  private String mDescription;

  public Todo() {
  }

  public Todo(String id, String title, String description) {
    mId = id;
    mTitle = title;
    mDescription = description;
  }

  public String getId() {
    return mId;
  }

  public String getTitle() {
    return mTitle;
  }

  public String getDescription() {
    return mDescription;
  }

  public void setId(String id) {
    mId = id;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

  public void setDescription(String description) {
    mDescription = description;
  }
}
