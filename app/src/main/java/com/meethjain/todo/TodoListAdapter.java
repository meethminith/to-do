package com.meethjain.todo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.meethjain.todo.model.Todo;
import com.meethjain.todo.common.adapter.BaseAdapter;
import java.util.List;

/**
 * Created on 26/07/17.
 *
 * @author Meeth D Jain
 */

public class TodoListAdapter extends BaseAdapter<Todo, TodoListAdapter.TodoHolder> {

  public TodoListAdapter() {
    super();
  }

  @Override
  public void onBindViewHolder(TodoHolder holder, int position) {
    Todo todo = getItem(position);
    holder.mTitleTextView.setText(todo.getTitle());
    holder.mDescriptionTextView.setText(todo.getDescription());
  }

  @Override
  public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_todo, parent, false);
    return new TodoHolder(view);
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  static class TodoHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_title)
    TextView mTitleTextView;
    @BindView(R.id.tv_desc)
    TextView mDescriptionTextView;

    public TodoHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
