package com.meethjain.todo.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.meethjain.todo.Utils;
import com.meethjain.todo.model.Todo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 26/07/17.
 *
 * @author Meeth D Jain
 */

public abstract class BaseFragment extends Fragment {

    protected static final String REF_TO_DO = "todo";

    private String mAppId;
    protected FirebaseDatabase mFirebaseDatabase;
    protected DatabaseReference mDatabaseReference;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference(REF_TO_DO);
    }


    protected void addTodo(Todo todo) {
        mAppId = Utils.getAppId(getActivity());
        Log.d("TAG", "App_ID: " + mAppId);
        if (Utils.appIsSet) {
            Map<String, Object> idValue = new HashMap<>();
            idValue.put(mAppId, null);
            mDatabaseReference.updateChildren(idValue);
            // mDatabaseReference.setValue(mAppId);
            Utils.appIsSet = false;
        }

        String todoKey = mDatabaseReference.push().getKey();
        todo.setId(todoKey);
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(todoKey, todo);
        mDatabaseReference
                .child(mAppId)
                .updateChildren(childUpdates);


//
//        if (Utils.appIsSet) {
//            //Push the id as key;
//            Map<String, Object> idValue = new HashMap<>();
//            idValue.put(mAppId, null);
//            mDatabaseReference.updateChildren(idValue);
//            // mDatabaseReference.setValue(mAppId);
//
//            String todoKey = mDatabaseReference.push().getKey();
//            todo.setId(todoKey);
//            Map<String, Object> childUpdates = new HashMap<>();
//            childUpdates.put(todoKey, todo);
//            mDatabaseReference
//                    .child(mAppId)
//                    .updateChildren(childUpdates);
//
//            // mDatabaseReference.child(mAppId).child(todoKey).setValue(todo);
//            Utils.appIsSet = false;
//        } else {
//            String todoKey = mDatabaseReference.push().getKey();
//            todo.setId(todoKey);
//            Map<String, Object> childUpdates = new HashMap<>();
//            childUpdates.put(todoKey, todo);
//            mDatabaseReference
//                    .child(mAppId)
//                    .updateChildren(childUpdates);
//        }
//        mDatabaseReference.child(mAppId)
//                .child(todo.getId())
//                .setValue(todo);
    }
}
