package com.meethjain.todo.common;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created on 26/07/17.
 *
 * @author Meeth D Jain
 */

public abstract class BaseActivity extends AppCompatActivity {

  protected void replaceFragment(Fragment fragment, @IdRes int resId, boolean addToBackStack) {
    FragmentTransaction fragmentTransaction = getSupportFragmentManager()
        .beginTransaction()
        .replace(resId, fragment, fragment.getTag());
    if (addToBackStack) {
      fragmentTransaction.addToBackStack(fragment.getTag());
    }
    try {
      fragmentTransaction.commit();
    } catch (IllegalStateException ise) {
      fragmentTransaction.commitAllowingStateLoss();
    }
  }
}
