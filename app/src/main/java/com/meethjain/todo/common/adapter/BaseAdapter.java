package com.meethjain.todo.common.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 26/07/17.
 *
 * @author Meeth D Jain
 */

public abstract class BaseAdapter<T, K extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<K> {

  private List<T> mItems;

  public BaseAdapter() {
    mItems = new ArrayList<>();
  }

  public BaseAdapter(List<T> items) {
    mItems = items;
  }

  @Override
  public int getItemCount() {
    return mItems.size();
  }

  protected T getItem(int position) {
    return mItems.get(position);
  }

  abstract public void onBindViewHolder(K holder, int position);
  abstract public K onCreateViewHolder(ViewGroup parent, int viewType);
  abstract public long getItemId(int position);

  public void addAll(List<T> items) {
    mItems.clear();
    mItems.addAll(items);
    notifyDataSetChanged();
  }
}
