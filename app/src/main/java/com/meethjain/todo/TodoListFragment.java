package com.meethjain.todo;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.meethjain.todo.common.BaseFragment;
import com.meethjain.todo.model.Todo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class TodoListFragment extends BaseFragment implements ValueEventListener {

  @BindView(R.id.rv_todo_list)
  RecyclerView mTodoRecyclerView;

  private TodoListAdapter mTodoListAdapter;

  public static TodoListFragment newInstance() {
    return new TodoListFragment();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_todo, container, false);
    ButterKnife.bind(this, rootView);

    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    mTodoRecyclerView.setLayoutManager(linearLayoutManager);
    mTodoListAdapter = new TodoListAdapter();
    mTodoRecyclerView.setAdapter(mTodoListAdapter);
    mDatabaseReference
        .child(Utils.getAppId(getContext()))
        .addValueEventListener(this);
    return rootView;
  }

  @Override
  public void onDataChange(DataSnapshot dataSnapshot) {
    ArrayList<Todo> todoList = new ArrayList<>();
    for (DataSnapshot msgSnapshot : dataSnapshot.getChildren()) {
      todoList.add(msgSnapshot.getValue(Todo.class));
    }
    mTodoListAdapter.addAll(todoList);
  }

  @Override
  public void onCancelled(DatabaseError databaseError) {
  }
}
