package com.meethjain.todo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.meethjain.todo.common.BaseFragment;
import com.meethjain.todo.model.Todo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddTodoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddTodoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddTodoFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    //private String mAppId;

    @BindView(R.id.et_title)
    EditText mTitleEditText;
    @BindView(R.id.et_desc)
    EditText mDescEditText;

    public static AddTodoFragment newInstance() {
        return new AddTodoFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Was not required to get the appID everytime.
//        mAppId = Utils.getAppId(getActivity());
//        Log.d("TAG", " AddToFragment: App_ID: " + mAppId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_todo, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.btn_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit: {
                onAddTodo();
                break;
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void onTodoAdded(Todo todo);
    }

    private void onAddTodo() {
        String title = mTitleEditText.getText().toString();
        String desc = mDescEditText.getText().toString();
        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(desc)) {
            //Todo's id is updated in the addTodo method , hence null is passed
            Todo todo = new Todo(null, title, desc);
            addTodo(todo);
            if (mListener != null) {
                mListener.onTodoAdded(todo);
            }
        } else {
            Utils.showToast(getActivity(), R.string.todo_enter_title_and_desc);
        }
    }
}

