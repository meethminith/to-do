package com.meethjain.todo;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.meethjain.todo.model.Todo;

/**
 * Created on 26/07/17.
 *
 * @author Meeth D Jain
 */

public class TodoApp extends Application {

  @Override
  public void onCreate() {
    super.onCreate();
    FirebaseApp.initializeApp(this);
    FirebaseDatabase.getInstance().setPersistenceEnabled(true);
  }
}
