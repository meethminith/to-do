package com.meethjain.todo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.meethjain.todo.common.BaseActivity;
import com.meethjain.todo.model.Todo;

public class TodoActivity extends BaseActivity implements AddTodoFragment.OnFragmentInteractionListener {

  @BindView(R.id.fab)
  FloatingActionButton mFab;
  @BindView(R.id.toolbar)
  Toolbar mToolbar;

  @BindView(R.id.container)
  FrameLayout mContainer;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_todo);
    ButterKnife.bind(this);
    setSupportActionBar(mToolbar);

    if (savedInstanceState == null) {
      Fragment fragment = TodoListFragment.newInstance();
      replaceFragment(fragment, R.id.container, false);
    }
  }

  @OnClick({R.id.fab})
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.fab: {
        replaceFragment(AddTodoFragment.newInstance(), R.id.container, true);
        mFab.setVisibility(View.GONE);
        break;
      }
    }
  }

  @Override
  public void onTodoAdded(Todo todo) {
    onBackPressed();
    mFab.setVisibility(View.VISIBLE);
    Snackbar.make(findViewById(android.R.id.content), R.string.saved, Snackbar.LENGTH_LONG);
  }
}
