package com.meethjain.todo;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.widget.Toast;

import static android.content.Context.MODE_APPEND;

/**
 * Created on 27/07/17.
 *
 * @author Meeth D Jain
 */

public class Utils {

    public static boolean appIsSet = false;

    public static String getAppId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("app", MODE_APPEND);
        String appId = sharedPreferences.getString("app_id", "");

        if (TextUtils.isEmpty(appId)) {
            //appId = UUID.randomUUID().toString();
            appId = Settings.Secure.getString(
                    context.getContentResolver(), Settings.Secure.ANDROID_ID);
            sharedPreferences.edit().putString("app_id", appId).apply();

            //TODO: You can check if the appid is already present in the database.
            //If yes,then dont create a new key in the database[just updateChildren], else create setValue and updateChildren
            appIsSet = true;
        }
        return appId;
    }

    public static void showToast(Context context, @StringRes int message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
}
